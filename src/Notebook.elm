module Notebook exposing
    ( Notebook
    , append
    , appendChild
    , display
    , empty
    , focusChildren
    , focusNext
    , focusParent
    , focusPrevious
    , path
    , prepend
    , prependChild
    , pull
    , push
    , removeItem
    , setText
    , swapNext
    , swapPrevious
    , text
    )

import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Tree exposing (Tree)
import Tree.Zipper as Zipper exposing (Zipper)


type alias Notebook =
    Zipper String


empty : Notebook
empty =
    ""
        |> Tree.singleton
        |> Zipper.fromTree


setText : String -> Notebook -> Notebook
setText value notebook =
    Zipper.replaceLabel value notebook


prependChild : String -> Notebook -> Notebook
prependChild value notebook =
    Zipper.mapTree
        (value
            |> Tree.singleton
            |> Tree.prependChild
        )
        notebook


appendChild : String -> Notebook -> Notebook
appendChild value notebook =
    Zipper.mapTree
        (value
            |> Tree.singleton
            |> Tree.appendChild
        )
        notebook


prepend : String -> Notebook -> Notebook
prepend value notebook =
    case Zipper.parent notebook of
        Nothing ->
            notebook

        Just _ ->
            Zipper.prepend (Tree.singleton value) notebook


append : String -> Notebook -> Notebook
append value notebook =
    case Zipper.parent notebook of
        Nothing ->
            notebook

        Just _ ->
            Zipper.append (Tree.singleton value) notebook


removeItem : Notebook -> Notebook
removeItem notebook =
    notebook
        |> Zipper.removeTree
        |> Maybe.withDefault empty


swapNext : Notebook -> Notebook
swapNext notebook =
    let
        sparseTree =
            notebook
                |> Zipper.map Just

        item =
            Zipper.tree sparseTree
    in
    sparseTree
        |> Zipper.replaceLabel Nothing
        |> Zipper.nextSibling
        |> Maybe.map (Zipper.append item)
        |> Maybe.andThen Zipper.nextSibling
        |> Maybe.andThen (Zipper.filterMap identity)
        |> Maybe.withDefault notebook


swapPrevious : Notebook -> Notebook
swapPrevious notebook =
    let
        sparseTree =
            notebook
                |> Zipper.map Just

        item =
            Zipper.tree sparseTree
    in
    sparseTree
        |> Zipper.replaceLabel Nothing
        |> Zipper.previousSibling
        |> Maybe.map (Zipper.prepend item)
        |> Maybe.andThen Zipper.previousSibling
        |> Maybe.andThen (Zipper.filterMap identity)
        |> Maybe.withDefault notebook


pull : Notebook -> Notebook
pull notebook =
    let
        item =
            Zipper.tree notebook

        grandparent =
            notebook
                |> Zipper.parent
    in
    case grandparent of
        Nothing ->
            notebook

        Just _ ->
            notebook
                |> Zipper.removeTree
                |> Maybe.map (Zipper.append item)
                |> Maybe.andThen Zipper.nextSibling
                |> Maybe.withDefault notebook


push : Notebook -> Notebook
push notebook =
    let
        item =
            notebook
                |> Zipper.tree
                |> Tree.map Just
    in
    notebook
        |> Zipper.map Just
        |> Zipper.replaceLabel Nothing
        |> Zipper.previousSibling
        |> Maybe.map (Zipper.mapTree (Tree.appendChild item))
        |> Maybe.andThen Zipper.lastChild
        |> Maybe.andThen (Zipper.filterMap identity)
        |> Maybe.withDefault notebook



-- Query


path : Notebook -> List String
path notebook =
    case Zipper.parent notebook of
        Nothing ->
            [ Zipper.label notebook ]

        Just parent ->
            Zipper.label notebook :: path parent


text : Notebook -> String
text notebook =
    Zipper.label notebook



-- Navigation


focusNext : Notebook -> Notebook
focusNext notebook =
    notebook
        |> Zipper.nextSibling
        |> Maybe.withDefault notebook


focusPrevious : Notebook -> Notebook
focusPrevious notebook =
    notebook
        |> Zipper.previousSibling
        |> Maybe.withDefault notebook


focusParent : Notebook -> Notebook
focusParent notebook =
    notebook
        |> Zipper.parent
        |> Maybe.withDefault notebook


focusChildren : Notebook -> Notebook
focusChildren notebook =
    notebook
        |> Zipper.firstChild
        |> Maybe.withDefault notebook



-- Display the notebook


type alias Visual =
    { highlighted : Bool
    , text : String
    }


display : Notebook -> Element msg
display notebook =
    notebook
        |> Zipper.map (Visual False)
        |> Zipper.mapLabel (\visual -> { visual | highlighted = True })
        |> Zipper.toTree
        |> Tree.restructure identity displayItem


displayItem : Visual -> List (Element msg) -> Element msg
displayItem item children =
    Element.column
        [ Element.padding 10
        , Element.spacing 5
        , Element.width Element.fill
        , Background.color
            (if item.highlighted then
                Element.rgb 1 1 0.6

             else
                Element.rgb 1 1 1
            )
        ]
        [ Element.text item.text
        , Element.column
            [ Element.spacing 5
            , Element.width Element.fill
            ]
            children
        ]
