module Main exposing
    ( Model
    , Msg(..)
    , init
    , main
    , subscriptions
    , update
    , view
    )

import AssocList as Dict exposing (Dict)
import Browser
import Browser.Dom as Dom
import Browser.Navigation as Navigation
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html.Attributes
import Keyboard exposing (RawKey)
import Notebook exposing (Notebook)
import Task
import Url exposing (Url)


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , onUrlRequest = UrlRequested
        , onUrlChange = UrlChanged
        }


type alias Model =
    { key : Navigation.Key
    , url : Url
    , editing : Bool
    , keyboard : List Keyboard.Key
    , notebook : Notebook
    }


type alias Flags =
    ()


init : Flags -> Url -> Navigation.Key -> ( Model, Cmd Msg )
init _ url key =
    ( { key = key
      , url = url
      , editing = False
      , keyboard = []
      , notebook =
            Notebook.empty
                -- Set top level text
                |> Notebook.setText "Hello, this is the first notebook"
                -- And first level children
                |> Notebook.appendChild "Item one"
                |> Notebook.appendChild "Item two"
                |> Notebook.appendChild "Everything is going fine"
                -- Focus on second child (Item two)
                |> Notebook.focusChildren
                |> Notebook.focusNext
                -- Add two second level children
                |> Notebook.appendChild "two - A"
                |> Notebook.appendChild "two - B"
                -- Focus on first one (two - A)
                |> Notebook.focusChildren
      }
    , Cmd.none
    )


type Msg
    = NoOp
    | UrlRequested Browser.UrlRequest
    | UrlChanged Url
    | TextInputChanged String
    | GotKeyboardMsg Keyboard.Msg


type alias Keymap =
    Dict Keyboard.Key (Model -> ( Model, Cmd Msg ))


keymaps :
    { normal : Keymap
    , edit : Keymap
    }
keymaps =
    { normal =
        Dict.empty
            -- Shifting focus
            |> Dict.insert
                (Keyboard.Character "j")
                (\model ->
                    ( { model
                        | notebook =
                            model.notebook |> Notebook.focusNext
                      }
                    , Cmd.none
                    )
                )
            |> Dict.insert
                (Keyboard.Character "k")
                (\model ->
                    ( { model
                        | notebook =
                            model.notebook |> Notebook.focusPrevious
                      }
                    , Cmd.none
                    )
                )
            |> Dict.insert
                (Keyboard.Character "l")
                (\model ->
                    ( { model
                        | notebook =
                            model.notebook |> Notebook.focusChildren
                      }
                    , Cmd.none
                    )
                )
            |> Dict.insert
                (Keyboard.Character "h")
                (\model ->
                    ( { model
                        | notebook =
                            model.notebook |> Notebook.focusParent
                      }
                    , Cmd.none
                    )
                )
            -- Moving items around
            |> Dict.insert
                (Keyboard.Character "J")
                (\model ->
                    ( { model
                        | notebook =
                            Notebook.swapNext model.notebook
                      }
                    , Cmd.none
                    )
                )
            |> Dict.insert
                (Keyboard.Character "K")
                (\model ->
                    ( { model
                        | notebook =
                            Notebook.swapPrevious model.notebook
                      }
                    , Cmd.none
                    )
                )
            |> Dict.insert
                (Keyboard.Character "L")
                (\model ->
                    ( { model
                        | notebook =
                            Notebook.push model.notebook
                      }
                    , Cmd.none
                    )
                )
            |> Dict.insert
                (Keyboard.Character "H")
                (\model ->
                    if
                        (model.notebook
                            |> Notebook.path
                            |> List.length
                        )
                            > 2
                    then
                        ( { model
                            | notebook =
                                Notebook.pull model.notebook
                          }
                        , Cmd.none
                        )

                    else
                        ( model, Cmd.none )
                )
            -- Adding and deleting items
            |> Dict.insert
                (Keyboard.Character "d")
                (\model ->
                    ( { model
                        | notebook =
                            model.notebook |> Notebook.removeItem
                      }
                    , Cmd.none
                    )
                )
            |> Dict.insert
                (Keyboard.Character "a")
                (\model ->
                    ( { model
                        | notebook =
                            model.notebook
                                |> Notebook.prependChild ""
                                |> Notebook.focusChildren
                        , editing = True
                      }
                    , focusTextInput
                    )
                )
            |> Dict.insert
                (Keyboard.Character "o")
                (\model ->
                    if
                        (model.notebook
                            |> Notebook.path
                            |> List.length
                        )
                            > 1
                    then
                        ( { model
                            | notebook =
                                model.notebook
                                    |> Notebook.append ""
                                    |> Notebook.focusNext
                            , editing = True
                          }
                        , focusTextInput
                        )

                    else
                        ( model, Cmd.none )
                )
            |> Dict.insert
                (Keyboard.Character "O")
                (\model ->
                    if
                        (model.notebook
                            |> Notebook.path
                            |> List.length
                        )
                            > 1
                    then
                        ( { model
                            | notebook =
                                model.notebook
                                    |> Notebook.prepend ""
                                    |> Notebook.focusPrevious
                            , editing = True
                          }
                        , focusTextInput
                        )

                    else
                        ( model, Cmd.none )
                )
            -- Switch to edit mode
            |> Dict.insert
                Keyboard.Enter
                (\model ->
                    ( { model
                        | editing = True
                      }
                    , focusTextInput
                    )
                )
    , edit =
        Dict.empty
            |> Dict.insert
                Keyboard.Escape
                (\model ->
                    ( { model
                        | editing = False
                      }
                    , Cmd.none
                    )
                )
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        UrlRequested (Browser.Internal url) ->
            ( model
            , url
                |> Url.toString
                |> Navigation.pushUrl model.key
            )

        UrlRequested (Browser.External href) ->
            ( model
            , Navigation.load href
            )

        UrlChanged url ->
            ( { model | url = url }
            , Cmd.none
            )

        TextInputChanged value ->
            ( { model | notebook = Notebook.setText value model.notebook }
            , Cmd.none
            )

        GotKeyboardMsg keyboardMsg ->
            let
                keymap : Keymap
                keymap =
                    if model.editing then
                        keymaps.edit

                    else
                        keymaps.normal

                ( pressed, changed ) =
                    Keyboard.updateWithKeyChange Keyboard.anyKeyOriginal keyboardMsg model.keyboard

                press =
                    changed
                        |> Maybe.andThen
                            (\change ->
                                case change of
                                    Keyboard.KeyDown key ->
                                        Just key

                                    _ ->
                                        Nothing
                            )
            in
            press
                |> Maybe.andThen (\key -> Dict.get key keymap)
                |> Maybe.map (\operation -> operation model)
                |> Maybe.withDefault ( model, Cmd.none )


focusTextInput : Cmd Msg
focusTextInput =
    "text-input"
        |> Dom.focus
        |> Task.attempt (\_ -> NoOp)


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Keyboard.subscriptions |> Sub.map GotKeyboardMsg
        ]



-- VIEW


view : Model -> Browser.Document Msg
view model =
    { title = "Org App"
    , body =
        [ model
            |> ui
            |> Element.layout
                [ Element.width Element.fill
                , Element.height Element.fill
                ]
        ]
    }


ui : Model -> Element Msg
ui model =
    [ model.notebook
        |> Notebook.path
        |> List.map String.lines
        |> List.map List.head
        |> List.map (Maybe.withDefault "")
        |> List.map Element.text
        |> List.map
            (Element.el
                [ Element.paddingXY 10 5
                , Border.rounded 3
                , Background.color (Element.rgb 0.5 0.5 0.5)
                , Font.color (Element.rgb 1 1 1)
                ]
            )
        |> List.reverse
        |> Element.wrappedRow
            [ Element.padding 10
            , Element.spacing 10
            , Font.size 12
            ]
    , if model.editing then
        Input.multiline
            [ "text-input"
                |> Html.Attributes.id
                |> Element.htmlAttribute
            , Element.height Element.fill
            ]
            { onChange = TextInputChanged
            , text = Notebook.text model.notebook
            , placeholder = Nothing
            , label = Input.labelHidden "Text of the item"
            , spellcheck = True
            }

      else
        model.notebook
            |> Notebook.display
    ]
        |> Element.column
            [ Element.padding 20
            , Element.width Element.fill
            , Element.height Element.fill
            ]
