// This script is responsible for starting the application.
// Keep is small and dumb. Extend index.js if need be.
// If https://github.com/parcel-bundler/parcel/issues/808 get's resolved then
// we should simply inline it in the HTML file.

import * as program from "."

program.init(
  document.getElementById("app-container")
)

