.PHONY: all
all: .installed lint test dist

.PHONY: install
install:
	@rm -rf .installed
	@make .installed

.installed: package.json package-lock.json 
	@echo "Dependencies files are newer than .installed; (re)installing."
	@npm clean-install
	@echo "This file is used by 'make' for keeping track of last install time. If package.json, package-lock.json or elm.json are newer then this file (.installed) then all 'make *' commands that depend on '.installed' know they need to run npm install first." \
		> .installed

# Testing and linting targets
.PHONY: lint
lint: .installed
	@npx elm-analyse

.PHONY: test
test: tests

.PHONY: tests
tests: .installed verify-examples coverage

.PHONY: coverage
coverage: .installed
	@npx elm-coverage --report codecov

.PHONY: verify-examples
verify-examples: .installed
	@npx elm-verify-examples

.coverage/codecov.json: .installed test

# Run development server
.PHONY: run
run: .installed
	@npx parcel src/index.html

.PHONY: codecov
codecov: .coverage/codecov.json
	npx codecov --disable=gcov --file=.coverage/codecov.json

# Build distribution files and place them where they are expected
.PHONY: dist
dist: .installed
	@npx parcel build \
		src/index.html \

# Nuke from orbit
clean:
	@rm -rf elm-stuff/ dist/ node_modules/
	@rm -f .installed
